# DAST Engineer-On-Rotation 

## Purpose

The engineer-on-rotation repository holds the source of truth for which DAST engineer is the engineer-on-rotation. More information about this process can be found in the [Dynamic Analysis Group](https://about.gitlab.com/handbook/engineering/development/sec/secure/dynamic-analysis/) section in the handbook.

## How it works

There should only ever be one direct member of this project, this person is the engineer-on-rotation. 

## How to find who is the engineer-on-rotation

### Using the UI

- Navigate to this project on GitLab.com.
- From the navigation menu, choose `Manage`, then `Members`.
- Sort the list by `Access Granted`, &darr; ascending.

The engineer-on-rotation is likely the most recent person added as a member, so will be displayed at the top. The `Source` should include `Direct member`.

### Using GraphiQL

- Navigate to https://gitlab.com/-/graphql-explorer
- Run the following query:
  ```graphql
  {
    project(fullPath: "gitlab-org/secure/dynamic-analysis/engineer-on-rotation") {
      projectMembers(relations: DIRECT) {
        nodes {
          user {
            id
            username
            name
          }
        }
      }
    }
  }
  ```
